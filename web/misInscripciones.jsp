<%@page import="java.util.List"%>
<%@page import="Database.ConnectionDB"%>
<%@page import="Database.AyudantesDB"%>
<%@page import="Entities.Ayudante"%>
<%@page import="java.sql.SQLException"%>
<%@page import="Entities.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Database.UsuariosDB"%>
<%@page import="Database.EventosDB"%>
<%@page import="Entities.Evento"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    List<Evento> eventos = (List<Evento>) request.getAttribute("listaEventosInscrito");
    List<Usuario> listaUsuarios = (List<Usuario>) request.getAttribute("listaUsuarioInscrito");
    int contador = 0;
%>

<!DOCTYPE html>
<html>
    <head>
        <title>CIVIS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="./styles/estilosNavBar.css">
        <link rel="stylesheet" href="./styles/estilosCarta.css">
        <link rel="stylesheet" href="./styles/estilosFormEvento.css">
    </head>
    <body>
        <!-- Inicio NavBar -->
        <jsp:include flush="true" page="navbarlogged.jsp"></jsp:include>
            <!-- Final NavBar -->

            <!-- Inicio visualizaciÃ³n eventos -->
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                <% for (Evento evento : eventos) {%>
                <div class="blog-card">
                    <div class="meta">
                        <div class="photo" style="background-image: url(./img/panoramic-bcn.jpg)"></div>
                        <ul class="details">

                            <li class="author"><%=evento.getUbicacion()%></li>

                            <li class="author"><%=evento.getNum_ayudante()%> Persona</li>

                            <li class="author"><%=evento.getFecha_evento()%></li>

                            <li class="author"><%=evento.getHora_evento()%></li>
                        </ul>
                    </div>
                    <div class="description">
                        <h1><%=evento.getTitulo()%></h1>
                        <br><br>
                        <h2><%
                            out.print(listaUsuarios.get(contador++).getUsuario());
                        %></h2>
                        <br>
                        <p>
                            <%=evento.getDescripcion()%>
                        </p>
                        <form action="ayudante?sa=eliminar" method="post">
                            <button type="submit" name="eliminar" value="<%=evento.getId_evento()%>" class="btn btn-danger">
                                ELIMINAR
                            </button>
                        </form>

                    </div>
                </div>
                <% }%>
            </div>
            <div class="col-md-2"></div>
        </div>
        <!-- Fin visualizaciÃ³n eventos -->
    </body>
</html>
