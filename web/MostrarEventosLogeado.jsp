<%@page import="java.util.List"%>
<%@page import="Database.ConnectionDB"%>
<%@page import="Database.UsuariosDB"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Entities.Usuario"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Entities.Evento"%>
<%@page import="Database.EventosDB"%>


<%
    List<Evento> listaEventos = (List<Evento>) request.getAttribute("listaEventos");
    List<Usuario> listaUsuario = (List<Usuario>) request.getAttribute("listaUsr");
    int contador = 0;
%>
<html>
    <head>
        <title>CIVIS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="./styles/estilosNavBar.css">
        <link rel="stylesheet" href="./styles/estilosCarta.css">
        <link rel="stylesheet" href="./styles/estilosFormEvento.css">
    </head>
    <body>
        <jsp:include flush="true" page="navbarlogged.jsp"></jsp:include>  
            <div class="row">


                <div class="col-md-2"></div>

                <div class="col-md-8">
                <% for (Evento evento : listaEventos) {
                %>

                <div class="blog-card">

                    <div class="meta">
                        <div class="photo" style="background-image: url(./img/panoramic-bcn.jpg)"></div>
                        <ul class="details">


                            <li class="author"><%out.println(evento.getUbicacion());%></li>

                            <li class="author"><%out.println(evento.getNum_ayudante());%> personas</li>

                            <li class="author"><%out.println(evento.getFecha_evento());%></li>

                            <li class="author"><%out.println(evento.getHora_evento());%></li>

                        </ul>
                    </div>
                    <div class="description">
                        <h1><%out.println(evento.getTitulo());%></h1>
                        <br><br>
                        <h2><%
                    out.print(listaUsuario.get(contador++).getUsuario()); %></h2>
                        <br>
                        
                        <p><%out.println(evento.getDescripcion());%></p>
                        <form action="ayudante?sa=inscribir" method="post">
                            <button type="submit" class="btn btn-success" name="inscribir" value="<%= evento.getId_evento()%>">
                                INSCRIBIRME
                            </button>
                        </form>

                    </div>

                </div>

                <%}%>
            </div>

            <div class="col-md-2"></div>

        </div>
    </body>
</html>
