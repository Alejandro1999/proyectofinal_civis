<%@page import="java.util.List"%>
<%@page import="Database.ConnectionDB"%>
<%@page import="Database.AyudantesDB"%>
<%@page import="Entities.Ayudante"%>
<%@page import="java.sql.SQLException"%>
<%@page import="Database.EventosDB"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Entities.Evento"%>
<%@page import="Database.UsuariosDB"%>
<%@page import="Entities.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
    List<Evento> eventosPropios = (List<Evento>) request.getAttribute("listaEventosCreadosPorMi");
    List<Usuario> listaUsers = (List<Usuario>) request.getAttribute("listaUsuarioEventosMios");
    List<Ayudante> listaAyu = (List<Ayudante>) request.getAttribute("listaAyudanteEventosMios");
    int contador = 0;
%>
<!DOCTYPE html>
<html>
    <head>
        <title>CIVIS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="./styles/estilosNavBar.css">
        <link rel="stylesheet" href="./styles/estilosCarta.css">
        <link rel="stylesheet" href="./styles/estilosFormEvento.css">
        <link rel="stylesheet" href="./cssfonts/css/all.css">
    </head>
    <body>

        <!-- Inicio NavBar -->
        <jsp:include flush="true" page="navbarlogged.jsp"></jsp:include>

            <!-- Final NavBar -->

            <!-- Inicio visualizaciÃ³n eventos -->
            <div class="row">

                <div class="col-md-2"></div>

                <div class="col-md-8">
                <% for (Evento evento : eventosPropios) { %>
                <div class="blog-card">
                    <div class="meta">
                        <div class="photo" style="background-image: url(./img/panoramic-bcn.jpg)"></div>
                        <ul class="details">
                            <li class="author"><%out.println(evento.getUbicacion());%></li>
                            <br>
                            <li class="author"><%out.println(evento.getNum_ayudante());%> Personas</li>
                            <br>
                            <li class="author"><%out.println(evento.getFecha_evento());%></li>
                            <br>
                            <li class="author"><%out.println(evento.getHora_evento());%></li>
                        </ul>
                    </div>
                    <div class="description">
                        <h1><%out.println(evento.getTitulo());%></h1>
                        <br><br>
                        <h2><%out.print(listaUsers.get(contador++).getUsuario());%></h2>
                        <br>
                        <p></p>
                        <!-- <a href="ayudante?sa=listInscritos"> -->
                        <div class="recuadroNombres">
                            <i class="fas fa-users" style="margin-bottom: 20px">GENTE INSCRITA</i>
                            <br>
                            <div style="overflow-y: scroll; height: 110px; ">
                                <%
                                    
                                %><span style = "color: black;"> 
                                    <%--                                    <% out.println(listaUsers.getUsuario().toUpperCase());
                                                                        %>
                                    --%>                                   &nbsp;se ha inscrito para ayudarte</span>
                                <br><br>

                            </div>
                        </div>

                        <form action="evento?sa=eliminar" method="post" align="center">
                            <button type="submit" name="eliminar" value="<%=evento.getId_evento()%>" class="btn btn-danger" >
                                ELIMINAR
                            </button>
                        </form>
                        <!--    </a> -->
                    </div>
                </div>
                <%}%>
            </div>

            <div class="col-md-2"></div>

        </div>
        <!-- Fin visualizaciÃ³n eventos -->

    </body>
</html>
