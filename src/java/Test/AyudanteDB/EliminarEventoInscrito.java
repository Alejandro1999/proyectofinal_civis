package Test.AyudanteDB;

import Database.AyudantesDB;
import Database.ConnectionDB;
import Entities.Ayudante;
import java.sql.SQLException;

public class EliminarEventoInscrito {

    public static void main(String[] args) {
        
        Ayudante ayudante = new Ayudante(9, 2);
        System.out.println(ayudante);
        
        AyudantesDB ayudDB = new AyudantesDB(new ConnectionDB());
        try {
            System.out.println(ayudDB.eliminarEventoInscrito(9, 2));
        } catch (SQLException ex){
            ex.printStackTrace();
        }
        
    }
    
}
