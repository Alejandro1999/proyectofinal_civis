package Test.EventosDB;

import Database.ConnectionDB;
import Database.EventosDB;
import Entities.Ayudante;
import Entities.Evento;
import java.sql.SQLException;

public class EliminarEvento {

    public static void main(String[] args) throws Exception {
        
        Ayudante ayudante = new Ayudante(9, 27);
        
        EventosDB evenDB = new EventosDB(new ConnectionDB());
        
        System.out.println(ayudante);
        
        try {
            System.out.println(evenDB.eliminarEvento(27));
        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }
    
}
