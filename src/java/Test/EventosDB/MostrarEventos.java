package Test.EventosDB;

import Database.ConnectionDB;
import Database.EventosDB;
import java.sql.SQLException;

public class MostrarEventos {

    public static void main(String[] args) throws Exception {

        EventosDB eventDB = new EventosDB(new ConnectionDB());
        try {
            System.out.println(eventDB.mostrarEventos());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}