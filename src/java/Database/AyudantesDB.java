    package Database;

import Entities.Ayudante;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AyudantesDB {

    ConnectionDB conDB;
    PreparedStatement ps;

    public AyudantesDB(ConnectionDB conDB) {
        this.conDB = conDB;
    }
    
    //Metodo para inscribirs a un ayudante
    public void inscribirAyudante(Ayudante a) throws SQLException {

        Connection con = conDB.conexion();

        ps = con.prepareStatement(
                "INSERT INTO ayudantes (id_usuario, id_evento, aceptado, confirmado) "
                + "VALUES (?, ?, ?, ?)");
        ps.setInt(1, a.getId_usuario());
        ps.setInt(2, a.getId_evento());
        ps.setBoolean(3, a.getAceptado());
        ps.setBoolean(4, a.getConfirmado());

        ps.executeUpdate();

        ps.close();
        con.close();
    }

    //Listar  nombres de usuarios inscritos a un evento
    public ArrayList<Ayudante> listInscritos(int id_evento) throws SQLException {
        Connection con = conDB.conexion();

        ArrayList<Ayudante> listaAyudantes = new ArrayList<>();

        ps = con.prepareStatement("SELECT u.id_usuario, u.usuario as usuario, a.id_evento "
                + "FROM ayudantes a JOIN usuarios u "
                + "ON a.id_usuario = u.id_usuario "
                + "WHERE id_evento = ?");
        ps.setInt(1, id_evento);

        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            listaAyudantes.add(new Ayudante(
                    rs.getInt("id_usuario"),
                    rs.getString("usuario"),
                    rs.getInt("id_evento")));
        }
        ps.close();
        con.close();
        return listaAyudantes;

    }   
    
    //Elimina la inscripción a dicho evento
    public boolean eliminarEventoInscrito (int idUser, int idEvento )throws SQLException {
        
        boolean eliminado = false;
        Connection con = conDB.conexion();
        
        String consulta = "DELETE FROM AYUDANTES WHERE id_usuario = ? AND id_evento = ?";
        
        try {
            ps = con.prepareStatement(consulta);
            ps.setInt(1, idUser);
            ps.setInt(2, idEvento);
            ps.executeUpdate();
            
            eliminado = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return eliminado;        
    }
    
//    public ArrayList<Ayudante> verInscritosEnEvento(int idEvento) throws SQLException {
//        Connection con = conDB.conexion();
//        
//        ArrayList<Ayudante> listaInscritos = new ArrayList<>();
//        
//        ps = con.prepareStatement("SELECT u.usuario from usuarios u INNER JOIN ayudantes a "
//                + "ON u.id_usuario = a.id_usuario WHERE id_evento = ?");
//        
//        ps.setInt(1, idEvento);
//        
//        ResultSet rs = ps.executeQuery();
//        
//        while (rs.next()) {            
//            listaInscritos.add(new Ayudante(
//            rs.getString("usuario"),
//            rs.getInt("id_evento")));
//        }
//        ps.close();
//        con.close();
//        return listaInscritos;
//        
//    }
}
