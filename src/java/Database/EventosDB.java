package Database;

import Entities.Evento;
import Entities.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EventosDB {

    ConnectionDB conDB;
    
    private static final Logger LOGGER = Logger.getLogger(EventosDB.class.getName());

    public EventosDB(ConnectionDB conDB) {
        this.conDB = conDB;
    }

    
    //Metodo para crear un evento
    public boolean crearEvento(Evento e) throws Exception {

        String consulta = "INSERT INTO eventos (titulo,ubicacion,hora_registro,fecha_registro, "
                + "hora_evento, fecha_evento, descripcion, num_ayudante,id_creador) "
                + "VALUES(?,?,?,?,?,?,?,?,?)";
        try (Connection con = conDB.conexion();
                PreparedStatement ps = con.prepareStatement(consulta)) {
            boolean existe = false;

            ps.setString(1, e.getTitulo());
            ps.setString(2, e.getUbicacion());
            ps.setTime(3, Time.valueOf(e.getHora_registro()));
            ps.setDate(4, Date.valueOf(e.getFecha_registro()));
            ps.setTime(5, Time.valueOf(e.getHora_evento()));
            ps.setDate(6, Date.valueOf(e.getFecha_evento()));
            ps.setString(7, e.getDescripcion());
            ps.setInt(8, e.getNum_ayudante());
            ps.setInt(9, e.getId_creador());

            existe = true;
            ps.executeUpdate();

            return existe;
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, "Error al crear el evento", ex);
            throw new Exception(ex);
        }
    }

    //Metodo para ver todos los eventos
    public ArrayList<Evento> mostrarEventos() throws Exception {

        String consulta = "SELECT * FROM eventos";
        ArrayList<Evento> eventos = new ArrayList<>();

        try (Connection con = conDB.conexion();
                PreparedStatement ps = con.prepareStatement(consulta)) {

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                eventos.add(new Evento(
                        rs.getInt("id_evento"),
                        rs.getString("titulo"),
                        rs.getString("ubicacion"),
                        rs.getString("hora_registro"),
                        rs.getString("fecha_registro"),
                        rs.getString("hora_evento"),
                        rs.getString("fecha_evento"),
                        rs.getString("descripcion"),
                        rs.getInt("num_ayudante"),
                        rs.getInt("id_creador")));
            }
            return eventos;

        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, "Error al crear el evento", ex);
            throw new Exception(ex);
        }
    }

    //Metodo para ver todos los eventos a los que un usuario esta inscrito
    public ArrayList<Evento> verEstadoInscripcion(Usuario u) throws Exception {

        String consulta = "SELECT e.id_creador, usuario, e.id_evento, titulo, descripcion, ubicacion, fecha_evento, "
                + "hora_evento, aceptado, confirmado, hora_registro, fecha_registro, num_ayudante "
                + "FROM ayudantes a "
                + "JOIN eventos e ON a.id_evento = e.id_evento "
                + "JOIN usuarios u ON u.id_usuario = e.id_creador "
                + "WHERE a.id_usuario = ?";
        ArrayList<Evento> eventos = new ArrayList<>();

        try (Connection con = conDB.conexion();
                PreparedStatement ps = con.prepareStatement(consulta)) {

            ps.setInt(1, u.getId_usuario());

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                eventos.add(new Evento(
                        rs.getInt("id_creador"),
                        rs.getString("usuario"),
                        rs.getInt("id_evento"),
                        rs.getString("titulo"),
                        rs.getString("descripcion"),
                        rs.getString("ubicacion"),
                        rs.getString("fecha_evento"),
                        rs.getString("hora_evento"),
                        rs.getBoolean("aceptado"),
                        rs.getBoolean("confirmado"),
                        rs.getString("fecha_registro"),
                        rs.getString("hora_registro"),
                        rs.getInt("num_ayudante")));
            }

            return eventos;
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, "Error al crear el evento", ex);
            throw new Exception(ex);
        }

    }

    //Metodo para listar todos los eventos que un usuario ha creado
    public ArrayList<Evento> listarEventosPropios(Usuario u) throws Exception {

        String consulta = "SELECT * FROM eventos WHERE id_creador = ?";
        ArrayList<Evento> eventos = new ArrayList<>();

        try (Connection con = conDB.conexion();
                PreparedStatement ps = con.prepareStatement(consulta)) {

            ps.setInt(1, u.getId_usuario());

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                eventos.add(new Evento(
                        rs.getInt("id_evento"),
                        rs.getString("titulo"),
                        rs.getString("ubicacion"),
                        rs.getString("hora_registro"),
                        rs.getString("fecha_registro"),
                        rs.getString("hora_evento"),
                        rs.getString("fecha_evento"),
                        rs.getString("descripcion"),
                        rs.getInt("num_ayudante"),
                        rs.getInt("id_creador")));
            }

            return eventos;
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, "Error al crear el evento", ex);
            throw new Exception(ex);
        }

    }
    
    //Metodo para listar todos los eventos que un usuario se ha inscrito
    public ArrayList<Evento> listarEventosALosQueEstoyInscrito(Usuario u) throws Exception {

        String consulta = "SELECT e.* FROM eventos e INNER JOIN ayudantes a "
                + "ON a.id_evento = e.id_evento WHERE a.id_usuario = ?";
        ArrayList<Evento> eventos = new ArrayList<>();

        try (Connection con = conDB.conexion();
                PreparedStatement ps = con.prepareStatement(consulta)) {

            ps.setInt(1, u.getId_usuario());

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                eventos.add(new Evento(
                        rs.getInt("id_evento"),
                        rs.getString("titulo"),
                        rs.getString("ubicacion"),
                        rs.getString("hora_registro"),
                        rs.getString("fecha_registro"),
                        rs.getString("hora_evento"),
                        rs.getString("fecha_evento"),
                        rs.getString("descripcion"),
                        rs.getInt("num_ayudante"),
                        rs.getInt("id_creador")));
            }

            return eventos;
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, "Error al crear el evento", ex);
            throw new Exception(ex);
        }

    }

    //Elimina la inscripción a dicho evento
    public boolean eliminarEvento(int idEvento) throws Exception {
        
        String consulta1 = "DELETE FROM ayudantes WHERE id_evento = ?";
        String consulta2 = "DELETE FROM eventos WHERE id_evento = ?";

        boolean eliminado = false;
        
        try (Connection con = conDB.conexion();
                PreparedStatement ps1 = con.prepareStatement(consulta1)) {
            
            ps1.setInt(1, idEvento);
            ps1.executeUpdate();
            
            PreparedStatement ps2 = con.prepareStatement(consulta2);
            ps2.setInt(1, idEvento);
            ps2.executeUpdate();
            
            eliminado = true;

            return eliminado;
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, "Error al crear el evento", ex);
            throw new Exception(ex);
        }
        
    }
}
