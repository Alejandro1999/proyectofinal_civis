package Database;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;
import javax.sql.DataSource;

public class ConnectionDB {

    public Connection conexion() throws SQLException {

        return ds.getConnection();
    }

    private final Logger LOGGER = Logger.getLogger(ConnectionDB.class.getName());

    private DataSource ds;

    /**
     * S'obtÃ© un DataSource a partir d'un OracleDataSource.
     *
     * @throws DAOException
     */
    public ConnectionDB() {

        MysqlDataSource myds = new MysqlDataSource();
        myds.setURL("jdbc:mysql://localhost:3306/voluntariobd?useSSL=false&serverTimezone=UTC");
        myds.setUser("root");
        myds.setPassword("root");

        ds = myds;
    }
}
