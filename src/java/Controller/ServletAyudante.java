/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Database.AyudantesDB;
import Database.ConnectionDB;
import Database.EventosDB;
import Database.UsuariosDB;
import Entities.Ayudante;
import Entities.Evento;
import Entities.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario
 */
@WebServlet(name = "ServletAyudante", urlPatterns = {"/ayudante"})
public class ServletAyudante extends HttpServlet {

    EventosDB eventoDB;
    AyudantesDB aydb;
    UsuariosDB usr;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String sa = request.getParameter("sa");

        switch (sa) {
            case "inscribir":
                int id_evento_inscribir = Integer.parseInt(request.getParameter("inscribir"));

                Usuario uaux = new Usuario();
                Ayudante ayinscribir = new Ayudante();
                String nombreAy = "";
                try {
                    Cookie cookie[] = request.getCookies();

                    for (Cookie c : cookie) {
                        if (c.getName().equals("uName")) {
                            nombreAy = c.getValue();
                        }

                    }
                    uaux = usr.verUsuario(nombreAy);

                    ayinscribir.setId_usuario(uaux.getId_usuario());
                    ayinscribir.setId_evento(id_evento_inscribir);

                    aydb.inscribirAyudante(ayinscribir);

                    request.setAttribute("ayudante", ayinscribir);
                    RequestDispatcher rd = request.getRequestDispatcher("./evento?sa=listJoinedEvents");
                    rd.forward(request, response);

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                break;

            case "eliminar":

                int eventoEliminarEvento = Integer.parseInt(request.getParameter("eliminar"));
//                int idEventoEliminar = Integer.parseInt((String) request.getAttribute("guardarIdEvento"));

                Ayudante ayudanteEliminar = new Ayudante();

                Usuario uaux3 = new Usuario();
                String nombreAyudanteEliminar = "";

                try {
                    Cookie cookie[] = request.getCookies();

                    for (Cookie c : cookie) {
                        if (c.getName().equals("uName")) {
                            nombreAyudanteEliminar = c.getValue();
                        }
                    }
                    uaux3 = usr.verUsuario(nombreAyudanteEliminar);
                    
                    boolean borrado = aydb.eliminarEventoInscrito(uaux3.getId_usuario(), eventoEliminarEvento);

                    

//                    if (borrado) {
//                        listaAyudantes.remove(eventoEliminarEvento);
//                    }
                    request.setAttribute("eliminar", eventoEliminarEvento);
                    RequestDispatcher rd = request.getRequestDispatcher("./evento?sa=listJoinedEvents");
                    rd.forward(request, response);

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                break;

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public void init() throws ServletException {
        super.init();
        ConnectionDB con = new ConnectionDB();
        //To change body of generated methods, choose Tools | Templates.
        eventoDB = new EventosDB(con);
        aydb = new AyudantesDB(con);
        usr = new UsuariosDB(con);
//To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
