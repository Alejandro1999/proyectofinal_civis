package Controller;

import Database.AyudantesDB;
import Database.ConnectionDB;
import Database.EventosDB;
import Database.UsuariosDB;
import Entities.Ayudante;
import Entities.Evento;
import Entities.Usuario;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario
 */
@WebServlet(urlPatterns = {"/evento"})
public class ServletEvento extends HttpServlet {

    EventosDB eventoDB;
    AyudantesDB aydb;
    UsuariosDB usr;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");

        String sa = request.getParameter("sa");

        ArrayList<Ayudante> listaAyudantes = new ArrayList<>();
        RequestDispatcher rd;

        switch (sa) {
            case "newEvent":
                //CREAR UN NUEVO EVENTO

                String tituloEvento = request.getParameter("titulo");
                String ubicacion = request.getParameter("localidad");
                String hora_evento = request.getParameter("hora");
                String fecha_evento = request.getParameter("fecha");
                String numPersonas = request.getParameter("numPersonas");
                String descripcion = request.getParameter("descripcion");
                hora_evento += ":00";

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                SimpleDateFormat sdf_f = new SimpleDateFormat("YYYY-MM-dd");
                String hora_registro = sdf.format(cal.getTime());
                String fecha_registro = sdf_f.format(cal.getTime());

                int np = Integer.parseInt(numPersonas);
                Cookie ck[] = request.getCookies();
                String nomUsuario = "";
                for (Cookie c : ck) {
                    if (c.getName().equals("uName")) {
                        nomUsuario = c.getValue();
                    }
                }

                //CREO UN OBJETO EVENTO Y EJECUTO EL METODO
                try {
                    Usuario userCookie = usr.verUsuario(nomUsuario);

                    Evento evento1 = new Evento(tituloEvento, ubicacion, hora_registro, fecha_registro, hora_evento, fecha_evento, descripcion, np, userCookie.getId_usuario());

                    eventoDB.crearEvento(evento1);

                } catch (SQLException ex) {
                    Logger.getLogger(ServletEvento.class.getName()).log(Level.SEVERE, null, ex);
                }

                //ENVIAMOS LA INFORMACION A LA SIGUIENTE PAGINA
                request.setAttribute("user", tituloEvento);
                rd = request.getRequestDispatcher("./evento?sa=showOwnEvents");
                rd.forward(request, response);
                break;

            case "listEvents":
                //VISUALIZAR TODOS LOS EVENTOS

                List<Evento> listaEventos;
                List<Usuario> listaUsuarios = new ArrayList<>();

                String log = (String) request.getParameter("log");

                try {
                    listaEventos = eventoDB.mostrarEventos();
                    for (Evento e : listaEventos) {
                        listaUsuarios.add(usr.verUsuarioId(e.getId_creador()));
                    }

                    request.setAttribute("listaUsr", listaUsuarios);
                    request.setAttribute("listaEventos", listaEventos);

                    String destino = "MostrarEventos.jsp";

                    if (log.equals("true")) {
                        destino = "MostrarEventosLogeado.jsp";
                    }

                    rd = request.getRequestDispatcher(destino);
                    rd.forward(request, response);

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                break;

            case "showOwnEvents":
                //VISUALIZAR LOS EVENTOS LOS CUALES HAS CREADO

                List<Usuario> listaUsuarioMisEventos = new ArrayList<>();
                List<Evento> listaEventosMisEventos = new ArrayList<>();

                String userName = "";

                try {
                    Cookie cookie[] = request.getCookies();

                    for (Cookie c : cookie) {
                        if (c.getName().equals("uName")) {
                            userName = c.getValue();
                        }
                    }

                    Usuario userEventsInscritos = usr.verUsuario(userName);
                    listaEventosMisEventos = eventoDB.listarEventosPropios(userEventsInscritos);

                    for (Evento e : listaEventosMisEventos) {
                        listaUsuarioMisEventos.add(usr.verUsuarioId(e.getId_creador()));
                    }

                    request.setAttribute("listaUsuarioEventosMios", listaUsuarioMisEventos);
                    request.setAttribute("listaEventosCreadosPorMi", listaEventosMisEventos);

                    rd = request.getRequestDispatcher("misEventos.jsp");
                    rd.forward(request, response);

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                break;

            case "listJoinedEvents":
                //VISUALIZAR LOS EVENTOS A LOS CUALES ESTAS INSCRITOS

                List<Ayudante> listaAyudanteInscrito = new ArrayList<>();
                List<Usuario> listaUsuarioInscrito = new ArrayList<>();
                List<Evento> listaEventosInscrito = new ArrayList<>();

                String usrName = "";

                try {
                    Cookie cookie[] = request.getCookies();

                    for (Cookie c : cookie) {
                        if (c.getName().equals("uName")) {
                            usrName = c.getValue();
                        }
                    }

                    Usuario userEventsInscritos = usr.verUsuario(usrName);
                    listaEventosInscrito = eventoDB.listarEventosALosQueEstoyInscrito(userEventsInscritos);

                    for (Evento e : listaEventosInscrito) {
                        listaUsuarioInscrito.add(usr.verUsuarioId(e.getId_creador()));
                    }

                    request.setAttribute("listaUsuarioInscrito", listaUsuarioInscrito);
                    request.setAttribute("listaEventosInscrito", listaEventosInscrito);
                    request.setAttribute("listaAyudanteInscritos", listaAyudanteInscrito);

                    rd = request.getRequestDispatcher("misInscripciones.jsp");
                    rd.forward(request, response);

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                break;

            case "eliminar":
                //ELIMINAR EL EVENTO CREADO POR TI MISMO

                int eventoEliminarEvento = Integer.parseInt(request.getParameter("eliminar"));
//                int idEventoEliminar = Integer.parseInt((String) request.getAttribute("guardarIdEvento"));

                Evento eventoEliminar = new Evento();

                Usuario uaux3 = new Usuario();
                String nombreAyudanteEliminar = "";

                try {
                    Cookie cookie[] = request.getCookies();

                    for (Cookie c : cookie) {
                        if (c.getName().equals("uName")) {
                            nombreAyudanteEliminar = c.getValue();
                        }
                    }
                    uaux3 = usr.verUsuario(nombreAyudanteEliminar);
                    eventoEliminar.setId_creador(uaux3.getId_usuario());
                    eventoEliminar.setId_evento(eventoEliminarEvento);

                    boolean borrado = eventoDB.eliminarEvento(eventoEliminarEvento);

//                    if (borrado) {
//                        listaAyudantes.remove(eventoEliminarEvento);
//                    }
                    request.setAttribute("eliminar", eventoEliminarEvento);
                    rd = request.getRequestDispatcher("./evento?sa=showOwnEvents");
                    rd.forward(request, response);

                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                break;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ServletEvento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void init() throws ServletException {
        super.init();
        ConnectionDB con = new ConnectionDB();
        //To change body of generated methods, choose Tools | Templates.
        eventoDB = new EventosDB(con);
        aydb = new AyudantesDB(con);
        usr = new UsuariosDB(con);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ServletEvento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
